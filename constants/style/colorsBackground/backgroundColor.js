const data = [
  { label: 'white', value: 'rgba(255, 255, 255)' },
  { label: 'black', value: 'rgba(0, 0, 0)' },
  { label: 'red', value: 'rgb(239, 68, 68)' },
]
const defaultValue = 'block'

export default {
  name: 'backgroundColor',
  label: 'Background color',
  input: 'select',
  category: 'colors-background',
  data,
  default: defaultValue,
}
