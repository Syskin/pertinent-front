import marginTop from '~/constants/style/boxModels/marginTop'
import marginBottom from '~/constants/style/boxModels/marginBottom'
import marginRight from '~/constants/style/boxModels/marginRight'
import marginLeft from '~/constants/style/boxModels/marginLeft'
import paddingTop from '~/constants/style/boxModels/paddingTop'
import paddingBottom from '~/constants/style/boxModels/paddingBottom'
import paddingRight from '~/constants/style/boxModels/paddingRight'
import paddingLeft from '~/constants/style/boxModels/paddingLeft'

export {
  marginTop,
  marginLeft,
  marginRight,
  marginBottom,
  paddingTop,
  paddingLeft,
  paddingRight,
  paddingBottom,
}
