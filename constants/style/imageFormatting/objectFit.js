const data = [
  { label: 'Contain', value: 'contain' },
  { label: 'Cover', value: 'cover' },
]
const defaultValue = 'contain'

export default {
  name: 'objectFit',
  label: 'Object Fit',
  input: 'select',
  category: 'image-formatting',
  data,
  default: defaultValue,
}
