import routes from '~/constants/routes.js'

export default (ctx, inject) => {
  inject('routes', routes)
}
