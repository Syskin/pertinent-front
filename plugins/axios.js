export default function ({ app, $axios, redirect }) {
  $axios.onError((error) => {
    if (error.response && error.response.status === 401) {
      app.$auth.reset()
    }
    return Promise.reject(error)
  })
}
