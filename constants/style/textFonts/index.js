import fontSize from '~/constants/style/textFonts/fontSize'
import textAlign from '~/constants/style/textFonts/textAlign'

export { fontSize, textAlign }
