const pluginName = 'pertinent-pages'
export default ($axios) => ({
  getOneById(id) {
    return $axios.$get(`/${pluginName}/page/${id}`)
  },
  getOneByUri(path) {
    let url = ``
    if (path) url = path
    return $axios.$post(`/${pluginName}/path`, { path: `/${url}` })
  },
  get() {
    return $axios.$get(`/${pluginName}/meta`)
  },
  create(data) {
    return $axios.$post(`/${pluginName}/create`, data)
  },
  updateOneById(id, data) {
    return $axios.$put(`/${pluginName}/page/${id}`, data)
  },
  deleteOneById(id) {
    return $axios.$delete(`/${pluginName}/page/${id}`)
  },
})
