const dataValues = require('../values/sizes')
const data = dataValues.values
const defaultValue = '0.5rem'
export default {
  name: 'paddingTop',
  label: 'Padding top',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
