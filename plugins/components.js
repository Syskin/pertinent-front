import Vue from 'vue'
import Button from '~/components/library/Button.vue'
import MainWrapper from '~/components/library/MainWrapper.vue'
import Paragraph from '~/components/library/Paragraph.vue'
import Picture from '~/components/library/Picture.vue'
import Link from '~/components/library/Link.vue'
import Dropdown from '~/components/library/Dropdown.vue'

const components = { Button, MainWrapper, Paragraph, Picture, Link, Dropdown }

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})

export default (context, inject) => {
  const componentsObjectConfig = {}
  Object.entries(components).forEach(([name, component]) => {
    const componentsKeys = [`name`, `style`, `extendConfig`, `props`]
    componentsObjectConfig[name] = {}
    componentsKeys.forEach((key) => {
      if (component && component[key])
        componentsObjectConfig[name][key] = component[key]
    })
  })
  inject(`componentsConfig`, componentsObjectConfig)
}
