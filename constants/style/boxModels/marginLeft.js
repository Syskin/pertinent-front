const dataValues = require('../values/sizes')
const data = dataValues.values
const defaultValue = '0.5rem'
export default {
  name: 'marginLeft',
  label: 'Margin left',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
