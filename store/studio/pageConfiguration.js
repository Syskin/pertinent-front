export const state = () => ({
  id: null,
  components: {},
  treeMap: [],
  lastUpdate: Date.now(),
})

export const mutations = {
  init(state) {
    state.id = null
    state.components = {}
    state.treeMap = []
  },
  setComponents(state, componentsMapped) {
    state.components = componentsMapped
    state.treeMap = buildTreeMap(state.components)
  },
  addComponent(state, component) {
    const { id, name, parentId, props, style, index, depth } = component
    if (id) {
      state.components[id] = {
        id,
        name,
        parentId,
        props,
        style,
        index,
        depth,
        lastUpdate: Date.now(),
      }
    }
    state.treeMap = buildTreeMap(state.components)
  },
  deleteComponent(state, componentId) {
    if (componentId && state.components[componentId])
      delete state.components[componentId]
    state.treeMap = []
    state.treeMap = buildTreeMap(state.components)
    console.log(state.treeMap)
  },
  updateComponent(state, data) {
    const { id, props, style } = data
    if (id) {
      if (props) state.components[id].props = props
      if (style) state.components[id].style = style
      state.components[id].lastUpdate = Date.now()
    }
    state.treeMap = []
    state.treeMap = buildTreeMap(state.components)
    state.lastUpdate = Date.now()
  },
  setConfiguration(state, config) {
    state.treeMap = []
    state.treeMap = config
  },
  setPageId(state, id) {
    state.id = id
  },
}

function buildTreeMap(components) {
  let treeMap = []

  const rootComponents = Object.keys(components).filter((id) => {
    return components[id].depth === 0
  })

  rootComponents.forEach((rootComponentId) => {
    const component = components[rootComponentId]
    component.children = getChildren(component, components, rootComponentId)
    delete component.childrenId
    treeMap.push(component)
  })
  treeMap = sortArray(treeMap, `index`)
  return treeMap
}

function getChildrenId(components, parentId) {
  if (!parentId) {
    return
  }
  return Object.keys(components).filter((id) => {
    return components[id].parentId === parentId
  })
}

function getChildren(component, components, parentId) {
  let children = []
  const childrenId = getChildrenId(components, parentId)
  if (childrenId && childrenId.length > 0) {
    childrenId.forEach((childId) => {
      if (!components[childId]) return
      const component = components[childId]
      component.children = getChildren(component, components, component.id)
      children.push(component)
      children = sortArray(children, `index`)
    })
  }
  return children
}

function sortArray(array, property) {
  return array.sort((a, b) => (a[property] > b[property] ? 1 : -1))
}
