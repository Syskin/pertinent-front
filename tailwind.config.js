module.exports = {
  theme: {
    extend: {
      zIndex: {
        100: '100',
      },
      minWidth: {
        '1/2': '50%',
        48: '12rem',
      },
      width: {
        min: 'min-content',
        inherit: 'inherit',
      },
      height: {
        72: '18rem',
        80: '20rem',
        88: '22rem',
        'fit-content': 'fit-content',
      },
      inset: {
        '-2.5': '-0.625rem',
        1.5: '0.375rem',
      },
      maxWidth: {
        inherit: 'inherit',
      },
      margin: {
        80: '20rem',
      },
      maxHeight: {
        0: '0',
        10: '2.5rem',
        20: '5rem',
        32: '8rem',
        40: '10rem',
        48: '12rem',
        56: '14rem',
        64: '16rem',
      },
      minHeight: {
        0: '0',
        8: '2rem',
        10: '2.5rem',
        20: '5rem',
        32: '8rem',
        40: '10rem',
        48: '12rem',
        56: '14rem',
        64: '16rem',
      },
    },
  },
}
