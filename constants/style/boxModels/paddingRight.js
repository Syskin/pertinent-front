const dataValues = require('../values/sizes')
const data = dataValues.values
const defaultValue = '0.5rem'
export default {
  name: 'paddingRight',
  label: 'Padding right',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
