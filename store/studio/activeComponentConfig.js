export const state = () => ({
  id: null,
  name: null,
  props: {},
  style: {},
  lastUpdate: Date.now(),
})

export const mutations = {
  init(state) {
    state.id = null
    state.name = null
    state.props = {}
    state.style = {}
  },
  setActiveComponentConfig(state, component) {
    const { id, props, name, style } = component
    state.id = id
    state.name = name
    state.props = props || {}
    state.style = style || {}
    state.lastUpdate = Date.now()
  },
  updateComponentProps(state, field) {
    const props = state.props
    state.props = null
    props[field.prop] = field.value
    state.props = props
    state.lastUpdate = Date.now()
  },
  updateComponentStyle(state, field) {
    const style = state.style
    state.style = null
    if (!style[field.screenSize]) style[field.screenSize] = {}
    style[field.screenSize][field.style] = field.value
    state.style = style
    state.lastUpdate = Date.now()
  },
}
