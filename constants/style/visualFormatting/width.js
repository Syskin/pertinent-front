const data = [
  { label: '1/4', value: '25%' },
  { label: '1/3', value: '33.3333%' },
  { label: '1/2', value: '50%' },
  { label: '2/3', value: '66.6666%' },
  { label: 'full', value: '100%' },
]
const defaultValue = '100%'

export default {
  name: 'width',
  label: 'Width',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
