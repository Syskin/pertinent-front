const dataValues = require('../values/sizes')
const data = dataValues.values
const defaultValue = '0.5rem'
export default {
  name: 'paddingBottom',
  label: 'Padding bottom',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
