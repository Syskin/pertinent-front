const data = [
  { label: 'flex', value: 'flex' },
  { label: 'block', value: 'block' },
  { label: 'inline-block', value: 'inline-block' },
]
const defaultValue = 'block'

export default {
  name: 'display',
  label: 'Display',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
