import pagesRepository from '~/api/repository/pages'
export default (ctx, inject) => {
  const pages = pagesRepository(ctx.$axios)

  inject('pages', pages)
}
