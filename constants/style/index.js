import {
  marginTop,
  marginLeft,
  marginRight,
  marginBottom,
  paddingTop,
  paddingLeft,
  paddingRight,
  paddingBottom,
} from '~/constants/style/boxModels'

import { backgroundColor } from '~/constants/style/colorsBackground'

import { objectFit } from '~/constants/style/imageFormatting'

import { fontSize, textAlign } from '~/constants/style/textFonts'

import {
  height,
  width,
  display,
  position,
} from '~/constants/style/visualFormatting'

export {
  backgroundColor,
  objectFit,
  height,
  width,
  display,
  position,
  marginTop,
  marginLeft,
  marginRight,
  marginBottom,
  paddingTop,
  paddingLeft,
  paddingRight,
  paddingBottom,
  fontSize,
  textAlign,
}
