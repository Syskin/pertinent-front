const data = [
  { label: '2px', value: '2px' },
  { label: '4px', value: '4px' },
  { label: '8px', value: '8px' },
  { label: '10px', value: '10px' },
  { label: '12px', value: '12px' },
  { label: '16px', value: '16px' },
  { label: '20px', value: '20px' },
  { label: '24px', value: '24px' },
  { label: '28px', value: '28px' },
  { label: '32px', value: '32px' },
  { label: '36px', value: '36px' },
]
const defaultValue = '16px'

export default {
  name: 'fontSize',
  label: 'Font size',
  input: 'select',
  category: 'text-fonts',
  data,
  default: defaultValue,
}
