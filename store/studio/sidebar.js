export const state = () => ({
  leftSideBar: false,
  downSideBar: false,
})

export const mutations = {
  updateLeftSideBar(state, bool) {
    state.leftSideBar = bool
  },
  updateDownSideBar(state, bool) {
    state.downSideBar = bool
  },
}
