export default {
  studio: {
    home: { name: 'Général', path: '/studio' },
    pages: { name: 'Gestion des pages', path: '/studio/pages' },
    page: { name: 'Configuration de page', path: '/studio/page/' },
  },
}
