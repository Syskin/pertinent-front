const dataValues = require('../values/sizes')
const data = dataValues.values
const defaultValue = '5rem'

export default {
  name: 'height',
  label: 'Height',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
