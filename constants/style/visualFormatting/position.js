const data = [
  { label: 'Absolute', value: 'absolute' },
  { label: 'Relative', value: 'relative' },
  { label: 'Fixed', value: 'fixed' },
  { label: 'Static', value: 'static' },
]
const defaultValue = 'static'

export default {
  name: 'position',
  label: 'Position',
  input: 'select',
  category: 'visual-formatting',
  data,
  default: defaultValue,
}
