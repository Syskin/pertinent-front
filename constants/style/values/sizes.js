const values = [
  { label: 'Auto', value: 'auto' },
  { label: 'Full', value: '100%' },
  { label: '0', value: '0' },
  { label: '4', value: '0.5rem' },
  { label: '8', value: '1rem' },
  { label: '16', value: '2rem' },
  { label: '32', value: '4rem' },
  { label: '64', value: '8rem' },
  { label: '128', value: '16rem' },
]
export { values }
