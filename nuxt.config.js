/* eslint no-template-curly-in-string: 0 */
export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: true,
  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: [
    '~/components/studio',
    '~/components/wrappers',
    '~/components/inputs',
    '~/components/form',
  ],

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'pertinent-front',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['./assets/scss/main.scss'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    './plugins/repository.js',
    '~plugins/components',
    '~plugins/directives',
    '~plugins/routes',
    './plugins/validation/email.js',
  ],

  server: {
    port: 3000,
    host: '0.0.0.0',
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    '@nuxtjs/auth-next',
    'nuxt-viewport',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    // '@nuxt/content',
  ],
  viewport: {
    breakpoints: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
    },

    defaultBreakpoints: {
      desktop: 'lg',
      mobile: 'xs',
      tablet: 'md',
    },

    fallbackBreakpoint: 'lg',
  },
  publicRuntimeConfig: {
    baseURL: '${BASE_URL}',
  },
  toast: {
    position: 'bottom-right',
    duration: 5000,
    register: [],
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: 'data.token',
          maxAge: 60 * 60 * 24,
          type: 'Bearer',
        },
        endpoints: {
          login: { url: 'admin/login', method: 'post', propertyName: false },
          refresh: false,
          logout: false,
          user: false,
        },
        user: {
          property: 'data.user',
          // autoFetch: true
        },
        tokenRequired: true,
        tokenType: 'Bearer',
      },
    },
    plugins: ['~plugins/axios.js'],
    redirect: {
      login: '/studio/login',
      logout: '/',
      callback: '/callback',
      home: '/',
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
