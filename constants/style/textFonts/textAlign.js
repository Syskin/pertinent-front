const data = [
  { label: 'Center', value: 'center' },
  { label: 'Left', value: 'left' },
  { label: 'Right', value: 'right' },
  { label: 'Justify', value: 'justify' },
]

const defaultValue = 'left'

export default {
  name: 'textAlign',
  label: 'Text align',
  input: 'select',
  category: 'text-fonts',
  data,
  default: defaultValue,
}
